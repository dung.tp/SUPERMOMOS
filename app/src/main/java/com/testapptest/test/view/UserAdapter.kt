package com.testapptest.test.view

import com.testapptest.test.R
import com.testapptest.test.base.adapter.BaseBindingAdapter
import com.testapptest.test.databinding.ViewUserBinding
import com.testapptest.test.model.GithubUser

class UserAdapter(private val onItemClick: (GithubUser) -> Unit) : BaseBindingAdapter<ViewUserBinding, GithubUser>() {
    override fun getLayoutId(viewType: Int) = R.layout.view_user

    override fun bindViewHolder(binding: ViewUserBinding, position: Int) {
        val data = list[position]
        binding.apply {
            githubUser = data
            viewParent.setOnClickListener{
                onItemClick.invoke(data)
            }
        }
    }

}