package com.testapptest.test.view

import com.testapptest.test.base.view.IBaseActivity
import com.testapptest.test.viewmodel.MainViewModel

interface IMainActivity : IBaseActivity<MainViewModel>{
    fun refreshContent()
}