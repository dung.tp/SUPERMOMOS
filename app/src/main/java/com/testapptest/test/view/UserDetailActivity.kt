package com.testapptest.test.view


import android.util.Log
import androidx.lifecycle.Observer
import com.pbreakers.mobile.androidtest.udacity.model.dao.UserDao
import com.testapptest.test.R
import com.testapptest.test.base.view.BaseActivity
import com.testapptest.test.databinding.ActivityMainBinding
import com.testapptest.test.databinding.ActivityUserDetailBinding
import com.testapptest.test.extension.getDefault
import com.testapptest.test.utils.Constants
import com.testapptest.test.viewmodel.UserDetailViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class UserDetailActivity : BaseActivity<ActivityUserDetailBinding, UserDetailViewModel>(), IUserDetailActivity{
    private val TAG = "UserDetailActivity"

    @Inject lateinit var userDao: UserDao

    override fun refreshContent() {
        val userID = intent.getStringExtra(Constants.KEY_PUT_OBJECT)
        mViewModel.getUserDetail(userID.getDefault())
        dataBinding.swiperefresh.isRefreshing = false
    }

    override fun getLayoutRes(): Int = R.layout.activity_user_detail
    override fun getModelClass(): Class<UserDetailViewModel> {
        return UserDetailViewModel::class.java
    }

    override fun initView() {
        super.initView()
        dataBinding.apply {
            iView = this@UserDetailActivity
        }
    }

    override fun initViewModel() {
        super.initViewModel()

        val userID = intent.getStringExtra(Constants.KEY_PUT_OBJECT)
        mViewModel.apply {
            getUserDetail(userID.getDefault())
            isCalledApi.observe(this@UserDetailActivity, Observer {
                //only observer after Called API
                if(it){
                    userDao.findUserDetail(intent.getStringExtra(Constants.KEY_PUT_OBJECT).getDefault()).observe(this@UserDetailActivity, Observer {
                        Log.d(TAG, "${it}")
                        if(it.isNullOrEmpty()){
                            onShowErrorDialog(getString(R.string.msg_no_network))
                        } else {
                            dataBinding.userDetail = it[0]
                        }
                    })
                }
            })
        }


    }
}