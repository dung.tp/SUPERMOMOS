package com.testapptest.test.view

import android.content.Intent
import android.util.Log
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.pbreakers.mobile.androidtest.udacity.model.dao.UserDao
import com.testapptest.test.R
import com.testapptest.test.application.MyApp
import com.testapptest.test.base.view.BaseActivity
import com.testapptest.test.databinding.ActivityMainBinding
import com.testapptest.test.extension.addDivider
import com.testapptest.test.extension.dp
import com.testapptest.test.extension.getDefault
import com.testapptest.test.extension.initLinear
import com.testapptest.test.model.GithubUser
import com.testapptest.test.utils.Constants
import com.testapptest.test.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(), IMainActivity {
    private val TAG = "MainActivity"

    @Inject
    lateinit var userDao: UserDao

    val mAdapter : UserAdapter by lazy {
        UserAdapter{ item ->
            val intent = Intent(this, UserDetailActivity::class.java)
            intent.putExtra(Constants.KEY_PUT_OBJECT, item.login)
            startActivity(intent)
        }
    }

    override fun refreshContent() {
        mViewModel.getUserList()
        dataBinding.swiperefresh.isRefreshing = false
    }

    override fun getLayoutRes(): Int = R.layout.activity_main
    override fun getModelClass(): Class<MainViewModel> {
        return MainViewModel::class.java
    }

    override fun initView() {
        super.initView()

        dataBinding.apply {
            iView = this@MainActivity
            rc_view.apply {
                initLinear(RecyclerView.VERTICAL)
                adapter = mAdapter
            }
        }
    }

    override fun initViewModel() {
        super.initViewModel()

        mViewModel.apply {
            getUserList()
            isCalledApi.observe(this@MainActivity, Observer {
                //only observer after Called API
                if(it){
                    userDao.findAll().observe(this@MainActivity, Observer {
                        Log.d(TAG, "${it}")
                        if(it.isNullOrEmpty()){
                            onShowErrorDialog(getString(R.string.msg_no_network))
                        } else {
                            mAdapter.updateData(it as MutableList<GithubUser>)
                        }
                    })
                }
            })
        }




    }
}