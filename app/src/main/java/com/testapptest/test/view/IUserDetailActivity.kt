package com.testapptest.test.view

import com.testapptest.test.base.view.IBaseActivity
import com.testapptest.test.viewmodel.UserDetailViewModel

interface IUserDetailActivity : IBaseActivity<UserDetailViewModel> {
    fun refreshContent()
}