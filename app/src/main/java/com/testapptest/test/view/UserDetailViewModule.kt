package com.testapptest.test.view

import com.testapptest.test.viewmodel.UserDetailViewModelModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [UserDetailViewModelModule::class])
abstract class UserDetailViewModule {
//    @FragmentScoped
//    @ContributesAndroidInjector
//    internal abstract fun contributeHomeFragment(): HomeFragment
}