package com.testapptest.test.base.view

import android.app.Activity
import android.content.Context
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.testapptest.test.base.viewmodel.BaseViewModel
import com.testapptest.test.data.preference.IConfigurationPrefs
import com.testapptest.test.model.error.ErrorMessage

interface IBaseView<T : BaseViewModel> {
    /**
     * Define the layout res id can be used to [Activity.setContentView]
     *
     * @return the layout res id
     */

    @LayoutRes
    fun getLayoutRes(): Int
    fun getModelClass(): Class<T>
    fun getViewModel(): T
    fun onDispatchTouchEvent()

    fun initView()
    fun initViewModel()
    fun showLoadingDialog()
    fun dismissLoadingDialog()
    fun handleError(errorMessage: ErrorMessage?)
    fun onHandleBackPressed()
    fun getToolbarTitle(): String?
    fun onEditTextChangedCallback(value: String?)
    fun getCurrentFragment(id: Int): BaseFragment<*, *>?
    fun getFragments(id: Int): MutableList<Fragment>?
    val configPrefs: IConfigurationPrefs
    val viewContext: Context
}