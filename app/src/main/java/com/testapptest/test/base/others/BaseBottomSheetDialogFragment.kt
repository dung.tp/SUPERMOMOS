package com.testapptest.test.base.others

import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.testapptest.test.R
import com.testapptest.test.base.view.BaseActivity
import com.testapptest.test.base.view.IBaseActivity
import com.testapptest.test.base.viewmodel.IBaseViewModel

abstract class BaseBottomSheetDialogFragment<V : ViewDataBinding>() :
    BottomSheetDialogFragment() {

    @LayoutRes
     abstract fun getLayoutRes() : Int
    protected lateinit var dataBinding: V


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.setOnShowListener { setupBottomSheet(it) }
        dataBinding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)
        return dataBinding.root
    }

    override fun onStart() {
        super.onStart()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.lifecycleOwner = this
        initView()
        initEvent()
    }

    private fun setupBottomSheet(dialogInterface: DialogInterface) {
        val bottomSheetDialog = dialogInterface as BottomSheetDialog
        val bottomSheet = bottomSheetDialog.findViewById<View>(R.id.design_bottom_sheet) ?: return
        bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        val behavior = BottomSheetBehavior.from(bottomSheet)
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
        behavior.peekHeight = 0
        behavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    dismiss()
                }
            }
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
        })
    }
    open fun initView() {
    }

    open fun initEvent() {}

    fun getCurrentFragment(id: Int): Fragment? {
        kotlin.runCatching {
            childFragmentManager.findFragmentById(id)?.childFragmentManager?.fragments?.let {
                if (it.isNotEmpty()) {
                    return it[0]
                }
            }
        }
        return null
    }

    fun getFragments(id: Int): MutableList<Fragment>? {
        kotlin.runCatching {
            childFragmentManager.findFragmentById(id)?.childFragmentManager?.fragments
        }
        return null
    }

    fun getParentActivity(): IBaseActivity<*> {
        return requireActivity() as BaseActivity<*, *>
    }

    fun getParentViewModel(): IBaseViewModel {
        return getParentActivity().getViewModel()
    }
}