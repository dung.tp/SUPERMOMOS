package com.testapptest.test.base.viewmodel

import androidx.lifecycle.LiveData
import com.testapptest.test.data.network.NetworkState

interface IBaseLoadMoreViewModel<T> : IBaseViewModel {
    val totalCountObs: LiveData<Int>
    val stateObs: LiveData<NetworkState>
    val isNewList: LiveData<Boolean>
    val listObs: LiveData<MutableList<T>>
    fun loadData(isRefreshed: Boolean = false, startKey: String? = null, isLoggedIn: Boolean = true)
    fun onReloadList()
    fun lastItem(): String
    fun isFirstPage(): Boolean
}