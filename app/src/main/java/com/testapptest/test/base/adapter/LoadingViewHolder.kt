package com.testapptest.test.base.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView


class LoadingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bindData() {}
}