package com.testapptest.test.base.others

import android.content.Context
import com.testapptest.test.data.preference.IConfigurationPrefs

interface IBaseDialogView {
    val viewContext: Context
    val configPrefs: IConfigurationPrefs
    fun getDisplayTitle(): String
    fun onClose()
}