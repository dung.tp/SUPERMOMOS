package com.testapptest.test.base.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.ResolveInfo
import android.graphics.Bitmap
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.ScrollView
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.testapptest.test.extension.gone
import com.testapptest.test.extension.visible
import com.testapptest.test.R
import com.testapptest.test.base.others.BaseCustomView
import com.testapptest.test.base.viewmodel.BaseViewModel
import com.testapptest.test.base.viewmodel.IBaseViewModel
import com.testapptest.test.customize.management.KeyboardHeightObserver
import com.testapptest.test.customize.management.KeyboardHeightProvider
import com.testapptest.test.customize.view.ToolbarLayout
import com.testapptest.test.data.preference.IConfigurationPrefs
import com.testapptest.test.extension.dp
import com.testapptest.test.model.error.ErrorMessage
import com.tbruyelle.rxpermissions2.RxPermissions
import dagger.android.support.DaggerFragment
import timber.log.Timber
import javax.inject.Inject


abstract class BaseFragment<V : ViewDataBinding, T : BaseViewModel> : DaggerFragment(),
    IBaseFragment<T>, KeyboardHeightObserver {

    private var isShowKeyboard: Boolean = false
    private var onGlobalLayoutListener: ViewTreeObserver.OnGlobalLayoutListener? = null
    override val navController by lazy { findNavController() }

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProvider.Factory

    lateinit var mViewModel: T

    protected lateinit var dataBinding: V

    private val keyboardHeightProvider: KeyboardHeightProvider by lazy {
        KeyboardHeightProvider(requireActivity())
    }

    private var spaceView: View? = null
    private var scrollView: ScrollView? = null

    override val configPrefs: IConfigurationPrefs
        get() = getParentActivity().configPrefs

    override val viewContext: Context
        get() = requireContext()

    override fun getViewModel(): T {
        return mViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this, viewModelProviderFactory).get(getModelClass())
        lifecycle.addObserver(mViewModel as LifecycleObserver)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSpaceView(view)
        dataBinding.lifecycleOwner = this
        initViewModel()
        initView()
        initEvent()
    }

    private fun initSpaceView(view: View) {
        spaceView = view.findViewWithTag(getString(R.string.tag_space_view))
        scrollView = view.findViewWithTag(getString(R.string.tag_scroll_view))
        dataBinding.root.post {
            keyboardHeightProvider.start()
        }
    }

    override fun initView() {
        context?.apply {
            view?.findViewWithTag<ToolbarLayout>(getString(R.string.tag_tool_bar_view))
                ?.initToolbar(this@BaseFragment)
        }
    }

    override fun getToolbarTitle(): String? = null

    open fun initEvent() {}

    override fun onEditTextChangedCallback(value: String?) {}

    override fun getCurrentFragment(id: Int): BaseFragment<*, *>? {
        kotlin.runCatching {
            childFragmentManager.findFragmentById(id)?.childFragmentManager?.fragments?.let {
                if (it.isNotEmpty()) {
                    return it[0] as? BaseFragment<*, *>
                }
            }
        }
        return null
    }

    override fun getFragments(id: Int): MutableList<Fragment>? {
        kotlin.runCatching {
            childFragmentManager.findFragmentById(id)?.childFragmentManager?.fragments
        }
        return null
    }

    override fun initViewModel() {
        mViewModel.apply {
            errorObs.observe(viewLifecycleOwner, Observer {
                it.message?.apply {
                    handleError(it)
                    resetErrorMessage()
                }
            })
            isLoadingObs.observe(viewLifecycleOwner, Observer {
                if (it) {
                    showLoadingDialog()
                } else {
                    dismissLoadingDialog()
                }
            })
        }
    }

    @SuppressLint("CheckResult")
    fun openGallery(requestCode: Int = GALLERY_REQUEST) {
        mViewModel.requestPermission(
            RxPermissions(this),
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            )
        ) { granted ->
            if (granted) {
                context?.apply {
                    var chooserIntent: Intent? = null
                    var intentList: MutableList<Intent> = ArrayList()
                    val pickIntent = Intent().apply {
                        type = "image/*"
                        action = Intent.ACTION_GET_CONTENT
                    }
                    val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    intentList = addIntentsToList(this, intentList, pickIntent)
                    intentList = addIntentsToList(this, intentList, takePhotoIntent)
                    if (intentList.size > 0) {
                        chooserIntent = Intent.createChooser(
                            intentList.removeAt(intentList.size - 1),
                            ""
                        )
                        chooserIntent.putExtra(
                            Intent.EXTRA_INITIAL_INTENTS,
                            intentList.toTypedArray()
                        )
                    }
                    startActivityForResult(chooserIntent, requestCode)
                }
            }
        }
    }

    private fun addIntentsToList(
        context: Context,
        list: MutableList<Intent>,
        intent: Intent
    ): MutableList<Intent> {
        val resInfo: List<ResolveInfo> =
            context.packageManager.queryIntentActivities(intent, 0)
        for (resolveInfo in resInfo) {
            val packageName = resolveInfo.activityInfo.packageName
            val targetedIntent = Intent(intent)
            targetedIntent.setPackage(packageName)
            list.add(targetedIntent)
        }
        return list
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            data?.data?.apply {
                onUriResult(this, requestCode)
                return
            }

            data?.extras?.get("data")?.apply {
                onBitmapResult(this as? Bitmap, requestCode)
            }
        }
    }

    open fun onUriResult(uri: Uri, requestCode: Int) {}

    open fun onBitmapResult(bitmap: Bitmap?, requestCode: Int) {}

    open fun onChooseImage(requestCode: Int, data: Intent?) {}

    override fun showLoadingDialog() {
        getParentActivity().showLoadingDialog()
    }

    override fun dismissLoadingDialog() {
        getParentActivity().dismissLoadingDialog()
    }

    override fun handleError(errorMessage: ErrorMessage?) {
        getParentActivity().handleError(errorMessage)
    }

    override fun onScrollToTop() {

    }

    override fun navigate(id: Int, bundle: Bundle?) {
        kotlin.runCatching {
            navController.navigate(id, bundle)
        }
    }

    override fun navigate(id: Int) {
        kotlin.runCatching {
            navController.navigate(id, null)
        }
    }

    override fun onKeyboardHeightChanged(height: Int, orientation: Int) {
        spaceView?.apply {
            layoutParams.height = height
            requestLayout()
            if (height > 0) {
                if (!isVisible) {
                    visible()
                    view?.rootView?.findFocus()?.let { focusView ->
                        scrollView?.smoothScrollTo(0, focusView.y.toInt())
                    }
                }
            } else {
                if (!isGone) {
                    gone()
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        keyboardHeightProvider.setKeyboardHeightObserver(null)
    }

    override fun onResume() {
        super.onResume()
        keyboardHeightProvider.setKeyboardHeightObserver(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        keyboardHeightProvider.close()
    }

    override fun onDispatchTouchEvent() {}

    override fun onHandleBackPressed() {
        getParentActivity().onHandleBackPressed()
    }

    override fun getParentActivity(): IBaseActivity<*> {
        return requireActivity() as BaseActivity<*, *>
    }

    override fun getParentViewModel(): IBaseViewModel {
        return getParentActivity().getViewModel()
    }

    companion object {
        const val GALLERY_REQUEST = 111
        const val CAMERA_REQUEST = 112
    }
}