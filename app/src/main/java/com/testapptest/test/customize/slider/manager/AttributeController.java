package com.testapptest.test.customize.slider.manager;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.testapptest.test.R;


public class AttributeController {

    private BannerOptions mBannerOptions;

    public AttributeController(BannerOptions bannerOptions) {
        mBannerOptions = bannerOptions;
    }

    public void init(@NonNull Context context, @Nullable AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SliderViewPager);
            initBannerAttrs(typedArray);
            typedArray.recycle();
        }
    }


    private void initBannerAttrs(TypedArray typedArray) {
        int interval = typedArray.getInteger(R.styleable.SliderViewPager_bvp_interval, 3000);
        boolean isAutoPlay = typedArray.getBoolean(R.styleable.SliderViewPager_bvp_auto_play, true);
        boolean isCanLoop = typedArray.getBoolean(R.styleable.SliderViewPager_bvp_can_loop, true);
        int pageMargin = (int) typedArray.getDimension(R.styleable.SliderViewPager_bvp_page_margin, 0);
        int roundCorner = (int) typedArray.getDimension(R.styleable.SliderViewPager_bvp_round_corner, 0);
        int revealWidth = (int) typedArray.getDimension(R.styleable.SliderViewPager_bvp_reveal_width, BannerOptions.DEFAULT_REVEAL_WIDTH);
        int pageStyle = typedArray.getInt(R.styleable.SliderViewPager_bvp_page_style, 0);
        int scrollDuration = typedArray.getInt(R.styleable.SliderViewPager_bvp_scroll_duration, 0);
        mBannerOptions.setInterval(interval);
        mBannerOptions.setAutoPlay(isAutoPlay);
        mBannerOptions.setCanLoop(isCanLoop);
        mBannerOptions.setPageMargin(pageMargin);
        mBannerOptions.setRoundRectRadius(roundCorner);
        mBannerOptions.setRightRevealWidth(revealWidth);
        mBannerOptions.setPageStyle(pageStyle);
        mBannerOptions.setScrollDuration(scrollDuration);
    }

}
