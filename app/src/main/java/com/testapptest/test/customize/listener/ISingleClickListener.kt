package com.testapptest.test.customize.listener

interface ISingleClickListener<T> {
    fun onSingleClicked(data: T)
}