package com.testapptest.test.customize.view

import android.content.Context
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.EditText
import androidx.databinding.ViewDataBinding
import com.testapptest.test.extension.getDefault
import com.testapptest.test.base.others.BaseCustomView
import com.testapptest.test.databinding.CustomTogglePasswordBinding

class TogglePasswordLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : BaseCustomView(context, attrs, defStyle) {

    lateinit var edt: EditText
    lateinit var binding: CustomTogglePasswordBinding

    override fun initViewDataBinging(inflater: LayoutInflater): ViewDataBinding {
        binding = CustomTogglePasswordBinding.inflate(inflater, this, true)
        return binding
    }

    fun onChangeToggleStatus() {
        val isSelected = binding.isShowPassword.getDefault()
        binding.isShowPassword = !isSelected

        edt.transformationMethod = if (isSelected) {
            PasswordTransformationMethod.getInstance()
        } else {
            HideReturnsTransformationMethod.getInstance()
        }
    }
}