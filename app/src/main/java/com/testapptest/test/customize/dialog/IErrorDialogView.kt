package com.testapptest.test.customize.dialog

import com.testapptest.test.base.others.IBaseDialogView


interface IErrorDialogView: IBaseDialogView {
    fun onErrorConfirmed()
}