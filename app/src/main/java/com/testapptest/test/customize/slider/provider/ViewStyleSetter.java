

package com.testapptest.test.customize.slider.provider;

import android.os.Build;
import android.view.View;

import androidx.annotation.RequiresApi;

public class ViewStyleSetter {

    private View mView;

    public ViewStyleSetter(View view) {
        this.mView = view;
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void setRoundRect(float radius) {
        this.mView.setClipToOutline(true);
        this.mView.setOutlineProvider(new RoundViewOutlineProvider(radius));
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void setOvalView() {
        this.mView.setClipToOutline(true);
        this.mView.setOutlineProvider(new OvalViewOutlineProvider());
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void clearShapeStyle() {
        this.mView.setClipToOutline(false);
    }
}
