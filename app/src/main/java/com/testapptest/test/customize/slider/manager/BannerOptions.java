package com.testapptest.test.customize.slider.manager;

import android.view.View;

import androidx.viewpager2.widget.ViewPager2;

import com.testapptest.test.customize.slider.constants.PageStyle;
import com.testapptest.test.customize.slider.utils.BannerUtils;

import static com.testapptest.test.customize.slider.transform.ScaleInTransformer.DEFAULT_MIN_SCALE;

public class BannerOptions {

    public BannerOptions() {
        pageMargin = BannerUtils.dp2px(20);
        rightRevealWidth = DEFAULT_REVEAL_WIDTH;
        leftRevealWidth = DEFAULT_REVEAL_WIDTH;
    }

    public static final int DEFAULT_REVEAL_WIDTH = -1000;

    private int offScreenPageLimit = ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT;

    private int interval;

    private boolean isCanLoop;

    private boolean isAutoPlay = false;

    private int pageMargin;

    private int rightRevealWidth;

    private int leftRevealWidth;

    private int pageStyle = PageStyle.NORMAL;

    private float pageScale = DEFAULT_MIN_SCALE;

    private int mIndicatorVisibility = View.VISIBLE;

    private int scrollDuration;

    private int roundRadius;

    private boolean userInputEnabled = true;

    private int orientation = ViewPager2.ORIENTATION_HORIZONTAL;

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public boolean isCanLoop() {
        return isCanLoop;
    }

    public void setCanLoop(boolean canLoop) {
        isCanLoop = canLoop;
    }

    public boolean isAutoPlay() {
        return isAutoPlay;
    }

    public void setAutoPlay(boolean autoPlay) {
        isAutoPlay = autoPlay;
    }

    public int getPageMargin() {
        return pageMargin;
    }

    public void setPageMargin(int pageMargin) {
        this.pageMargin = pageMargin;
    }

    public int getRightRevealWidth() {
        return rightRevealWidth;
    }

    public void setRightRevealWidth(int rightRevealWidth) {
        this.rightRevealWidth = rightRevealWidth;
    }

    public int getLeftRevealWidth() {
        return leftRevealWidth;
    }

    public void setLeftRevealWidth(int leftRevealWidth) {
        this.leftRevealWidth = leftRevealWidth;
    }



    public int getPageStyle() {
        return pageStyle;
    }

    public void setPageStyle(int pageStyle) {
        this.pageStyle = pageStyle;
    }

    public float getPageScale() {
        return pageScale;
    }

    public void setPageScale(float pageScale) {
        this.pageScale = pageScale;
    }

    public int getRoundRectRadius() {
        return roundRadius;
    }

    public void setRoundRectRadius(int roundRadius) {
        this.roundRadius = roundRadius;
    }

    public int getScrollDuration() {
        return scrollDuration;
    }

    public void setScrollDuration(int scrollDuration) {
        this.scrollDuration = scrollDuration;
    }

    public int getIndicatorVisibility() {
        return mIndicatorVisibility;
    }

    public void setIndicatorVisibility(int indicatorVisibility) {
        mIndicatorVisibility = indicatorVisibility;
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public boolean isUserInputEnabled() {
        return userInputEnabled;
    }

    public void setUserInputEnabled(boolean userInputEnabled) {
        this.userInputEnabled = userInputEnabled;
    }

    public int getOffScreenPageLimit() {
        return offScreenPageLimit;
    }

    public void setOffScreenPageLimit(int offScreenPageLimit) {
        this.offScreenPageLimit = offScreenPageLimit;
    }
}
