package com.testapptest.test.customize.slider.constants;

import com.testapptest.test.customize.slider.SliderViewPager;

public interface PageStyle {

    int NORMAL = 0;
    /**
     * @deprecated please use {@link SliderViewPager#setRevealWidth(int)} instead.
     */
    @Deprecated
    int MULTI_PAGE = 1 << 1;
    /**
     * Requires Api Version >= 21
     */
    int MULTI_PAGE_OVERLAP = 1 << 2;

    int MULTI_PAGE_SCALE = 1 << 3;
}
