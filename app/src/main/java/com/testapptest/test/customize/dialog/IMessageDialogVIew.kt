package com.testapptest.test.customize.dialog

import com.testapptest.test.base.others.IBaseDialogView


interface IMessageDialogVIew: IBaseDialogView {
    fun onLeftConfirmAction()
    fun onRightConfirmAction()
    override fun getDisplayTitle(): String = ""
}