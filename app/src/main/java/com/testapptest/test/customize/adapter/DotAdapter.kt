package com.testapptest.test.customize.adapter

import com.testapptest.test.R
import com.testapptest.test.base.adapter.BaseBindingAdapter
import com.testapptest.test.databinding.ViewDotBinding


class DotAdapter : BaseBindingAdapter<ViewDotBinding, Int>() {
    private var currentFocus = 0

    override fun getLayoutId(viewType: Int): Int = R.layout.view_dot

    override fun bindViewHolder(binding: ViewDotBinding, position: Int) {
        binding.apply {
            when (currentFocus) {
                position -> {
                    color = "#FFEB5757"
                    colorPercent = 100
                }
                else -> {
                    color = "#FFFFFFFF"
                    colorPercent = 50
                }
            }
        }
    }

    fun updateCurrentFocus(position: Int) {
        currentFocus = position
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = list.size
}
