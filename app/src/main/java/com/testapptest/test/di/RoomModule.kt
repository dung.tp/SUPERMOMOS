package com.testapptest.test.di


import android.app.Application
import androidx.room.Room
import com.pbreakers.mobile.androidtest.udacity.model.dao.UserDao
import com.testapptest.test.model.dao.AppDatabase
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {
    @Singleton
    @Provides
    open fun providesRoomDatabase(mApplication: Application): AppDatabase {
        return Room.databaseBuilder(mApplication, AppDatabase::class.java, "demo-db").build()
    }

    @Singleton
    @Provides
    open fun providesProductDao(appDatabase: AppDatabase): UserDao {
        return appDatabase.userDao
    }
}