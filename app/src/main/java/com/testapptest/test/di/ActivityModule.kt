package com.testapptest.test.di

import com.testapptest.test.di.scope.ActivityScoped
import com.testapptest.test.view.MainActivity
import com.testapptest.test.view.MainViewModule
import com.testapptest.test.view.UserDetailActivity
import com.testapptest.test.view.UserDetailViewModule
import dagger.Module
import dagger.android.ContributesAndroidInjector



@Module(includes = [ViewModelModule::class])
abstract class ActivityModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [MainViewModule::class])
    internal abstract fun contributeMainActivity(): MainActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [UserDetailViewModule::class])
    internal abstract fun contributeUserDetailActivity(): UserDetailActivity

}