package com.testapptest.test.data.network

import java.io.IOException


class NoConnectionException(msg: String) : IOException(msg)