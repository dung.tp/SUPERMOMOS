package com.testapptest.test.data

import com.testapptest.test.model.GithubUser
import com.testapptest.test.model.UserDetail
import io.reactivex.rxjava3.core.Single
import retrofit2.http.*


interface RetrofitAPIs {

    @GET("users")
    fun getUserList(): Single<List<GithubUser>>

    @GET("users/{userId}")
    fun getUserDetail(
        @Path("userId") userId : String
    ): Single<UserDetail>
}