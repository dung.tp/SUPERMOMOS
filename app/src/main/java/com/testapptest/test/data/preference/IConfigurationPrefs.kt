package com.testapptest.test.data.preference


interface IConfigurationPrefs {
    var token: String?
    var language: String
    fun clear()
}