package com.testapptest.test.utils.event

class SystemEvent {



    class Pairing(var status: Status? = null, var type: Type? = null, val message: String = "") {

        fun isConnected() = status == Status.CONNECTED

        enum class Type {
            FIND_PEER,
            DEVICE_CONNECT,
            REQUEST_DATA;
        }

        enum class Status {
            CONNECTING,
            CONNECTED,
            DISCONNECTED;


            companion object {
                fun getByValue(value: Boolean?) = if (value == true) {
                    CONNECTED
                } else {
                    DISCONNECTED
                }
            }
        }
    }


    class SocketData(val data: Any)

}