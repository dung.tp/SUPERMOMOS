package com.testapptest.test.utils


class UsKey {
    companion object {
        val accessToken:String = "KeyAccessToken"
        val refreshToken:String = "KeyRefreshToken"
        val logoutToken:String = "KeyLogoutToken"
        val allowBiometric:String = "KeyAllowBiometric"
        val email:String = "KeyEmail"
        val phone:String = "KeyPhone"
    }
}