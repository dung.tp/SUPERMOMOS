package com.testapptest.test.utils


import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import com.testapptest.test.extension.getDefault
import com.testapptest.test.utils.Constants.EMPTY
import timber.log.Timber
import java.io.*
import kotlin.math.roundToInt


class DecreaseImageSizeUtils {

    fun decreaseImageSize(imagePath: String? = null, bitmap: Bitmap? = null,listener: (String) -> Unit) {
        var decreaseBitmap: Bitmap? = null
        var urlImage: String
        try {
            val scaled = bitmap ?: decodeSampledBitmapFromResourceMemOpt(
                FileInputStream(
                    File(
                        imagePath ?: EMPTY
                    )
                ) /*, o.outWidth / scale, o.outHeight / scale*/
            )
            decreaseBitmap = if (scaled != null) {
                val mat = Matrix()
                if (imagePath != null) {
                    mat.postRotate(getOrientationFromExif(imagePath).toFloat())
                }
                Bitmap.createBitmap(scaled, 0, 0, scaled.width, scaled.height, mat, true)
            } else {
                null
            }
        } catch (e: FileNotFoundException) {
            urlImage = EMPTY
            Timber.e("FileNotFoundException: ${e.message}")
        } catch (e: OutOfMemoryError) {
            urlImage = EMPTY
            Timber.e("OutOfMemoryError: ${e.message}")
        }
        urlImage = saveImageToFile(decreaseBitmap)
        listener(urlImage)
    }


    private fun decodeSampledBitmapFromResourceMemOpt(
        inputStream: InputStream
        /*, int reqWidth, int reqHeight*/
    ): Bitmap? {
        var byteArr = ByteArray(0)
        val buffer = ByteArray(1024)
        var len = -1
        var count = 0

        return try {
            while (inputStream.read(buffer).also { len = it } > -1) {
                if (len != 0) {
                    if (count + len > byteArr.size) {
                        val newbuf = ByteArray((count + len) * 2)
                        System.arraycopy(byteArr, 0, newbuf, 0, count)
                        byteArr = newbuf
                    }
                    System.arraycopy(buffer, 0, byteArr, count, len)
                    count += len
                }
            }
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true
            BitmapFactory.decodeByteArray(byteArr, 0, count, options)
            options.inSampleSize = calculateInSampleSize(options /*, reqWidth, reqHeight*/)
            options.inJustDecodeBounds = false
            options.inPreferredConfig = Bitmap.Config.ARGB_8888
            BitmapFactory.decodeByteArray(byteArr, 0, count, options)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            null
        }
    }

    private fun calculateInSampleSize(options: BitmapFactory.Options /*, int reqWidth, int reqHeight*/): Int {
        // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        // The new size we want to scale to
        if (height >= width) {
            if (height > REQUIRED_SIZE) {
                inSampleSize = (height / REQUIRED_SIZE).toFloat().roundToInt()
            }
        } else {
            if (width > REQUIRED_SIZE) {
                inSampleSize = (width / REQUIRED_SIZE).toFloat().roundToInt()
            }
        }
        return inSampleSize
    }

    private fun getOrientationFromExif(imagePath: String): Int {
        var orientation = 0
        try {
            val exif = ExifInterface(imagePath)
            val exifOrientation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL
            )
            when (exifOrientation) {
                ExifInterface.ORIENTATION_ROTATE_270 -> orientation = 270
                ExifInterface.ORIENTATION_ROTATE_180 -> orientation = 180
                ExifInterface.ORIENTATION_ROTATE_90 -> orientation = 90
                ExifInterface.ORIENTATION_NORMAL -> orientation = 0
                else -> {
                }
            }
        } catch (e: IOException) {
        }
        return orientation
    }

    private fun saveImageToFile(bitmap: Bitmap? = null): String {
        return try {
            val file = FileUtils.getTempFile()
            file.createNewFile()
            val fOut = FileOutputStream(file)
            bitmap?.apply {
                this.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
            }
            fOut.flush()
            fOut.close()
            file?.absolutePath.getDefault()
        } catch (e: Exception) {
            Timber.e("Exception: ${e.message}")
            EMPTY
        }
    }

    companion object {
        private const val REQUIRED_SIZE = 1024
    }
}