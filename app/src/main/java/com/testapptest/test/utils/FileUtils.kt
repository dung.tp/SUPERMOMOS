package com.testapptest.test.utils

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.widget.Toast
import com.testapptest.test.utils.Constants.EMPTY
import com.testapptest.test.utils.Constants.PATTERN_DATE_FULL_YEAR
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset
import java.text.SimpleDateFormat
import java.util.*

object FileUtils {

    private const val DEFAULT_IMAGE_FOLDER = "haas_image"
    private const val DEFAULT_DOWNLOAD_FOLDER = "haas_download"
    private const val SUFFIX_IMAGE = ".jpg"
    private const val PACKAGE_EXTERNAL_STORAGE = "com.android.externalstorage.documents"
    private const val PACKAGE_GOOGLE_PHOTO = "com.google.android.apps.photos.content"
    private const val URI_DOWNLOAD = "content://downloads/public_downloads"
    private const val PACKAGE_MEDIA_PROVIDER = "com.android.providers.media.documents"
    private const val PACKAGE_DOWNLOAD_PROVIDER = "com.android.providers.downloads.documents"
    private const val IMAGE = "image"
    private const val CONTENT = "content"
    private const val FILE_KEY = "file"
    private const val PRIMARY_KEY = "primary"
    private const val STORAGE_KEY = "storage"

    /**
     * Check file is exist or not
     */
    fun isExistFile(context: Context, fileName: String): File {
        val path = FileUtils.getExternalFolder(context, DEFAULT_DOWNLOAD_FOLDER)
        val file = File(path, fileName)
        return file
    }


    /**
     * @param context the context use for Content Resolver
     * @param uri The Uri to check.
     * @return  path of file from uri
     */
    fun getRealPathFromURI(context: Context, uri: Uri): String? {
        when {
            // DocumentProvider
            DocumentsContract.isDocumentUri(context, uri) -> {
                when {
                    // ExternalStorageProvider
                    isExternalStorageDocument(uri) -> {
                        val docId = DocumentsContract.getDocumentId(uri)
                        val split = docId.split(":").toTypedArray()
                        val type = split[0]
                        // This is for checking Main Memory
                        return if (PRIMARY_KEY.equals(type, ignoreCase = true)) {
                            if (split.size > 1) {
                                Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                            } else {
                                Environment.getExternalStorageDirectory().toString() + "/"
                            }
                            // This is for checking SD Card
                        } else {
                            STORAGE_KEY + "/" + docId.replace(":", "/")
                        }
                    }
                    isDownloadsDocument(uri) -> {
                        val fileName = getFilePath(context, uri)
                        if (fileName != null) {
                            return Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName
                        }
                        var id = DocumentsContract.getDocumentId(uri)
                        if (id.startsWith("raw:")) {
                            id = id.replaceFirst("raw:".toRegex(), EMPTY)
                            val file = File(id)
                            if (file.exists()) return id
                        }
                        val contentUri = ContentUris.withAppendedId(
                            Uri.parse(URI_DOWNLOAD),
                            java.lang.Long.valueOf(id)
                        )
                        return getDataColumn(context, contentUri, null, null)
                    }
                    isMediaDocument(uri) -> {
                        val docId = DocumentsContract.getDocumentId(uri)
                        val split = docId.split(":").toTypedArray()
                        val type = split[0]
                        var contentUri: Uri? = null
                        if (IMAGE == type) {
                            contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        }
                        val selection = "_id=?"
                        val selectionArgs = arrayOf(split[1])
                        return getDataColumn(context, contentUri, selection, selectionArgs)
                    }
                }
            }
            CONTENT.equals(uri.scheme, ignoreCase = true) -> {
                // Return the remote address
                return if (isGooglePhotosUri(uri)) uri.lastPathSegment
                else getDataColumn(context, uri, null, null)
            }
            FILE_KEY.equals(uri.scheme, ignoreCase = true) -> {
                return uri.path
            }
        }
        return null
    }

    /**
     * @param context the context use for Content Resolver
     * @param uri The Uri to check.
     * @param selection The Selection criteria
     * @return a column
     */
    private fun getDataColumn(
        context: Context, uri: Uri?, selection: String?,
        selectionArgs: Array<String>?
    ): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(
            column
        )
        try {
            if (uri == null) return null
            cursor = context.contentResolver.query(
                uri, projection, selection, selectionArgs,
                null
            )
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }


    /**
     * @param context the context use for Content Resolver
     * @param uri The Uri to check.
     * @return file path
     */
    private fun getFilePath(context: Context, uri: Uri?): String? {
        var cursor: Cursor? = null
        val projection = arrayOf(
            MediaStore.MediaColumns.DISPLAY_NAME
        )
        try {
            if (uri == null) return null
            cursor = context.contentResolver.query(
                uri, projection, null, null,
                null
            )
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return PACKAGE_EXTERNAL_STORAGE == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private fun isDownloadsDocument(uri: Uri): Boolean {
        return PACKAGE_DOWNLOAD_PROVIDER == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private fun isMediaDocument(uri: Uri): Boolean {
        return PACKAGE_MEDIA_PROVIDER == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return PACKAGE_GOOGLE_PHOTO == uri.authority
    }

    /**
     * Get path folder for storage
     */
    private fun getTempFolder(): File {
        val directoryFolder =
            File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),
                DEFAULT_IMAGE_FOLDER
            )
        directoryFolder.mkdirs()
        return directoryFolder
    }

    /**
     * Get path file for storage
     */
    fun getTempFile(): File {
        val timeStamp = SimpleDateFormat(PATTERN_DATE_FULL_YEAR, Locale.getDefault()).format(Date())
        return File(
            getTempFolder().absolutePath,
            IMAGE.plus(Calendar.getInstance().timeInMillis).plus(timeStamp).plus(SUFFIX_IMAGE)
        )
    }

    /**
     * Get External Folder
     */
    private fun getExternalFolder(context: Context, folder: String = DEFAULT_IMAGE_FOLDER): File? {
        try {
            val state = Environment.getExternalStorageState()
            return when {
                Environment.MEDIA_MOUNTED == state -> {
                    val file = File(
                        Environment.getExternalStorageDirectory(),
                        folder
                    )
                    file.mkdir()
                    file
                }
                Environment.MEDIA_MOUNTED_READ_ONLY == state -> {
                    Toast.makeText(
                        context, "Can not write on external storage.",
                        Toast.LENGTH_LONG
                    ).show()
                    null
                }
                else -> {
                    null
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return null
    }

    /**
     * Delete draft image file
     */

    fun deleteImageFile() {
        val directoryFolder = File(
            Environment.getExternalStorageDirectory(),
            DEFAULT_IMAGE_FOLDER
        )
        if (directoryFolder.exists()) {
            val deleteCmd = "rm -r $directoryFolder"
            val runtime = Runtime.getRuntime()
            try {
                runtime.exec(deleteCmd)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }


    fun getJsonFromAsset(
        context: Context,
        fileName: String?
    ): String? {
        var json = EMPTY
        try {
            val stream: InputStream = context.assets.open(fileName!!)
            val size: Int = stream.available()
            val buffer = ByteArray(size)
            stream.read(buffer)
            stream.close()
            json = String(buffer, Charset.forName("UTF-8"))
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return json
    }
}
