package com.pbreakers.mobile.androidtest.udacity.model.dao

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.testapptest.test.model.GithubUser
import com.testapptest.test.model.UserDetail
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

@Dao
interface UserDao {

    @Query("SELECT * FROM users")
    fun findAll(): LiveData<List<GithubUser>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(users: List<GithubUser>)

    @Query("DELETE FROM users")
    fun deleteAllFromTable()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addUserDetail(userDetail: UserDetail)

    @Query("SELECT * FROM userDetails WHERE login = :userId ")
    fun findUserDetail(userId : String): LiveData<List<UserDetail>>

    fun insertUserListIntoDB(users: List<GithubUser>) {
        Observable.fromCallable {
            Runnable {
                deleteAllFromTable()
                add(users)
            }.run()
        }
            .subscribeOn(Schedulers.io())
            .subscribe {
                Log.d("UserDao", "insertUserListIntoDB")
            }
    }

    fun insertUserDetailIntoDB(user: UserDetail) {
        Log.d("UserDao", "insertUserDetailIntoDB ${user}")
        Observable.fromCallable {
            Runnable {
                addUserDetail(user)
            }.run()
        }
            .subscribeOn(Schedulers.io())
            .subscribe {
                Log.d("UserDao", "insertUserDetailIntoDB")
            }
    }

}