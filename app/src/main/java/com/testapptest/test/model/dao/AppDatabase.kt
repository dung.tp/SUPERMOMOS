package com.testapptest.test.model.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import com.pbreakers.mobile.androidtest.udacity.model.dao.UserDao
import com.testapptest.test.model.GithubUser
import com.testapptest.test.model.UserDetail

@Database(entities = [GithubUser::class, UserDetail::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract val userDao: UserDao
}