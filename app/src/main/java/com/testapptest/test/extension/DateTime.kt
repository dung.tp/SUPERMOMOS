package com.testapptest.test.extension

import android.content.Context
import com.testapptest.test.R
import com.testapptest.test.utils.Constants
import timber.log.Timber
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*



fun String?.timeFormat(dateTimeFormat: String?, pattern: String = Constants.PATTERN_PARSE_TIME_SERVER): String {
    kotlin.runCatching {
        var timeFormat = dateTimeFormat
        if (timeFormat.isNullOrEmpty()) {
            timeFormat = "dd MMM yyyy"
        }
        val millis = this.convertStringToMillis(pattern, isApplyTimezone = true)
        return if (millis != 0.toLong()) {
            val cal = Calendar.getInstance()
            cal.timeInMillis = millis
            val format = SimpleDateFormat(timeFormat, Locale.getDefault())
            format.format(cal.time)
        } else {
            "-"
        }
    }
    return "-"
}

fun Long?.timeFormat(
        dateTimeFormat: String
): String {
    try {
        return if (this != 0.toLong()) {
            val cal = Calendar.getInstance()
            cal.timeInMillis = this.getDefault()
            val format = SimpleDateFormat(dateTimeFormat, Locale.ENGLISH)
            format.timeZone = TimeZone.getTimeZone("UTC")
            format.format(cal.time)
        } else {
            "-"
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return "-"
}

fun String?.convertStringToMillis(
    format: String = Constants.PATTERN_PARSE_TIME_SERVER,
    isApplyTimezone: Boolean = true
): Long {
    if (this.isNullOrEmpty()) {
        return 0
    }
    val simpleDateFormat = SimpleDateFormat(format, Locale.getDefault())
    if (isApplyTimezone) {
        simpleDateFormat.timeZone = TimeZone.getTimeZone("GMT")
    }

    return try {
        simpleDateFormat.parse(this)?.time ?: 0
    } catch (ignored: ParseException) {
        0
    }
}

