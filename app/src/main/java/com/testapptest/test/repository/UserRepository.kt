package com.testapptest.test.repository

import android.util.Log
import com.pbreakers.mobile.androidtest.udacity.model.dao.UserDao
import com.testapptest.test.data.RetrofitAPIs
import com.testapptest.test.model.GithubUser
import com.testapptest.test.model.UserDetail
import io.reactivex.rxjava3.core.Single
import retrofit2.Retrofit
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val service: RetrofitAPIs
){

    fun getUserList(): Single<List<GithubUser>> {
        return service.getUserList()
    }

    fun getUserDetail(userId: String): Single<UserDetail> {
        return service.getUserDetail(userId)
    }
}