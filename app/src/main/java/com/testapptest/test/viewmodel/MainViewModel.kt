package com.testapptest.test.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.pbreakers.mobile.androidtest.udacity.model.dao.UserDao
import com.testapptest.test.base.viewmodel.BaseViewModel
import com.testapptest.test.model.GithubUser
import com.testapptest.test.repository.UserRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val context: Context,
    private val repository: UserRepository,
    private val userDao: UserDao
): BaseViewModel(){
    private val TAG = "MainViewModel"
    private val _isCalledApi = MutableLiveData<Boolean>(false)
    val isCalledApi : LiveData<Boolean>
        get() {
            return  _isCalledApi
        }

    fun getUserList() {
        addDisposable(repository.getUserList()
            .doOnSubscribe{
                setLoading(true)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally{
                setLoading(false)
                _isCalledApi.postValue(true)
            }
            .map {
                Log.d(TAG, "map ${it}")
                userDao.insertUserListIntoDB(it)
            }
            .subscribe(
                {
                    Log.d(TAG, "subscribe ${it}")
                },
                {
                    Log.d(TAG,"error ${it.toString()}")
//                    onApiError(it.message)
                }
            )
        )
    }

}