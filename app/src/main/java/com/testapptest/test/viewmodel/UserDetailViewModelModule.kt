package com.testapptest.test.viewmodel

import androidx.lifecycle.ViewModel
import com.testapptest.test.di.scope.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap



@Module
abstract class UserDetailViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(UserDetailViewModel::class)
    abstract fun bindUserDetailViewModel(viewModel: UserDetailViewModel): ViewModel
}